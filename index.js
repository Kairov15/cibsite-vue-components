import Loader from './src/appComponents/Loader.vue'
import DepositCalc from './src/appComponents/DepositCalc.vue'
import AcquiringCalc from './src/appComponents/AcquiringCalc.vue'
import CommentPageForm from './src/appComponents/CommentPageForm.vue'
import CreditCalc from './src/appComponents/CreditCalc.vue'
export { Loader, DepositCalc, AcquiringCalc, CommentPageForm, CreditCalc }
