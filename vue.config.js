const debug = process.env.NODE_ENV !== 'production'
const WebpackAssetsManifest = require('webpack-assets-manifest')
const path = require('path');
module.exports = {
    filenameHashing: true,
    lintOnSave: false,
    productionSourceMap: debug,
    css: {
        sourceMap: debug,
        extract: false
    },
    configureWebpack: {
        plugins: [
            new WebpackAssetsManifest()
        ]
    },
    chainWebpack: config => {
        config.module
            .rule('scss')
            .oneOf('vue')
            .use('resolve-url-loader')
            .loader('resolve-url-loader').options({
            keepQuery: true,
            sourceMap: true
        })
            .before('sass-loader')

        config.module
            .rule('scss')
            .oneOf('vue')
            .use('sass-loader')
            .loader('sass-loader')
            .tap(options => ({
                ...options,
                sourceMap: true
            }))
        // config.module
        //     .rule('svg')
        //     .use('file-loader')
        //     .loader('file-loader')
        //     .tap(options => ({
        //         ...options,
        //         outputPath: 'css',
        //         esModule: false
        //     }))
    }
}
