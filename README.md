# Vue компоненты для сайта банка

### Установка
```
yarn install
```


### Компиляция в режиме разработки с локальным сервером
```
yarn run serve
```

### Запуст ESlint

```
yarn run lint
```

### Компиляция в продакшн
Компиляция производится в gitLab CI/CD при обновлении ветки master, а затем происходит копирование директории dist на тестовый сервер cib-beta.dev.cinet.ru в папку /var/www/cib-beta.dev.cinet.ru/www/public_html/apps/cibLibrary

### Внедрение на бой
Как было сказано выше, при обновлении ветки мастер, происходит компиляция и копирование на тестовый сайт cib-beta.dev.cinet.ru. Для внедрения изменений в продуктив, необходимо из директории /var/www/cib-beta.dev.cinet.ru/www/public_html/apps/cibLibrary запустить скрипт copy_to_prod.sh
```
bash copy_to_prod.sh
```
А затем ввести ssh пароль от сервера сайта банка www.centrinvest.ru (пароль можно найти в teamPass)

### Компиляция на тестовых сайтах
На тестовых сайтах cib{N}.dev.cinet.ru в директории /var/www/cib{N}.dev.cinet.ru/www/public_html/apps/cibLibrary запустить билд (если такой директории нет, создать её и инициализировать репозиторий) и запустить команду
```
yarn run lib
```

В файле `vue.config.js` можно изменить конфигурацию webpack - подробности в документации [Vue CLI 3](https://cli.vuejs.org/ru/guide/webpack.html).
