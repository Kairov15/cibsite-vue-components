import axios from 'axios'

const debug = process.env.NODE_ENV !== 'production' || window.location.host !== 'www.centrinvest.ru'
const SUGGESTIONS_BASE_URL = `${debug ? 'https://showroom25.dev.cinet.ru/' : '/'}cabinet/api/suggestions`
const AUTOFILL_BASE_URL = `${debug ? 'https://showroom25.dev.cinet.ru/' : '/'}cabinet/api/autofill`

const SERVICE_TYPE_DADATA_ORGANIZATION = 'dadata.organization'
const SERVICE_TYPE_DADATA_FMS_ISSUED_NAME = 'dadata.fmsUnit'
const SERVICE_TYPE_FIRST_NAME = 'fio.firstName'
const SERVICE_TYPE_LAST_NAME = 'fio.lastName'
const SERVICE_TYPE_PATRONYMIC = 'fio.patronymic'
const SERVICE_TYPE_FIO_FULL = 'fio.fullName'
const SERVICE_TYPE_AHUNTER_ADDRESS = 'ahunter.address'
const SERVICE_TYPE_DADATA_ADDRESS = 'dadata.address'
const SERVICE_TYPE_EMAIL = 'email'
const SERVICE_TYPE_PROFESSION = 'profession'
const SERVICE_TYPE_DADATA_BANK = 'dadata.bank'
const SERVICE_TYPE_POSTCODE = 'postcode'

const SuggestionsApiClient = {
    async suggestions (requestParams) {
        try {
            const response = await axios.get(SUGGESTIONS_BASE_URL, {
                params: requestParams,
                timeout: 5000
            })
            return response
        } catch (e) {
            return e.response
        }
    },
    async suggestionsPost (requestParams, queryParams) {
        const params = new URLSearchParams(queryParams).toString()
        try {
            const response = await axios.post(`${SUGGESTIONS_BASE_URL}?${params}`, requestParams, {
                timeout: 5000
            })
            return response
        } catch (e) {
            return e.response
        }
    },
    companyNameSuggestions (params) {
        const filtered = params.query.replace(/_/g, '')
        const requestParams = {
            ...params,
            query: filtered,
            service: SERVICE_TYPE_DADATA_ORGANIZATION,
            suggestionType: params.suggestionType || ''
        }
        return this.suggestions(requestParams)
    },
    fioFirstNameSuggestions (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_FIRST_NAME
        }
        return this.suggestions(requestParams)
    },
    fioLastNameSuggestions (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_LAST_NAME
        }
        return this.suggestions(requestParams)
    },
    fioPatronymicSuggestions (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_PATRONYMIC
        }
        return this.suggestions(requestParams)
    },
    fioFullNameSuggestions (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_FIO_FULL
        }
        return this.suggestions(requestParams)
    },
    ahunterAddressSuggestions (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_AHUNTER_ADDRESS
        }
        return this.suggestions(requestParams)
    },
    dadataAddressSuggestions (params) {
        const requestParams = {
            ...params
        }
        const queryParams = {
            service: SERVICE_TYPE_DADATA_ADDRESS
        }
        return this.suggestionsPost(requestParams, queryParams)
    },
    emailSuggestions (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_EMAIL
        }
        return this.suggestions(requestParams)
    },
    professionSuggestions (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_PROFESSION
        }
        return this.suggestions(requestParams)
    },
    dadataBankSuggestions (params) {
        const filtered = params.query.replace(/_/g, '')
        const requestParams = {
            ...params,
            query: filtered,
            service: SERVICE_TYPE_DADATA_BANK
        }
        return this.suggestions(requestParams)
    },
    postcodeSuggestions (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_POSTCODE
        }
        return this.suggestions(requestParams)
    },
    passportIssuedName (params) {
        const requestParams = {
            ...params,
            service: SERVICE_TYPE_DADATA_FMS_ISSUED_NAME
        }
        return this.suggestions(requestParams)
    }
}

export {
    SuggestionsApiClient
}
