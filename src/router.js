import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
    base: process.env.BASE_URL,
    mode: 'history',
    routes: [{
        path: '/',
        name: 'Главная',
        component: () => import('./views/Home.vue')
    },
        {
            path: '/comment',
            name: 'Коммент форма',
            component: () => import('./appComponents/CommentPageForm.vue')
        },
        {
            path: '/feedBackWidget',
            name: 'Виджет',
            component: () => import('./appComponents/FeedBackWidget')
        },
        {
            path: '/feedback',
            name: 'Форма обратной связи',
            component: () => import('./appComponents/FeedbackForm.vue')
        },
        {
            path: '/depositCalc',
            name: 'Калькулятор вкладов',
            component: () => import('./appComponents/DepositCalc.vue')
        },
        {
            path: '/newsCalendar',
            name: 'Новостной календарь',
            component: () => import('./appComponents/NewsCalendar')
        },
        {
            path: '/incomeReductionCalc',
            name: 'Калькулятор расчета снижения дохода',
            component: () => import('./appComponents/IncomeReductionCalc')
        },
        {
            path: '/creditCalc',
            name: 'Кредитный калькулятор',
            component: () => import('./appComponents/CreditCalc.vue')
        },
        {
            path: '/acquiringCalc',
            name: 'Калькулятор эквайринг',
            component: () => import('./appComponents/AcquiringCalc.vue')
        },
        {
            path: '/testPage',
            name: 'Тестовая страница',
            component: () => import('./appComponents/testInput')
        },
        {
            path: '/feebackpage',
            name: 'Страница формы обратной связи',
            component: () => import('./appComponents/FeedBackPage')
        },
        {
            path: '/vote',
            name: 'Голосование',
            component: () => import('./appComponents/Vote')
        },
        {
            path: '/maternalCapitalRequest',
            name: 'Заявка на материский капитал',
            component: () => import('./appComponents/MaternalCapitalRequest')
        }
    ]
})
