class Deposit {
    constructor (name, minSum, rate, cap, repl, part, pens, periodPayment, cur, link, rateQ, procQuart, procHalfYear) {
        this.name = name
        this.minSum = minSum
        this.rate = rate
        this.cap = cap
        this.repl = repl
        this.part = part
        this.pens = pens
        this.periodPayment = periodPayment
        this.cur = cur
        this.link = link
        this.rateQ = rateQ
        this.profitFinal = 0
        this.profit = []
        this.profitQ = []
        this.msg = []
        this.capitalization = ''
        this.procQuart = procQuart
        this.procHalfYear = procHalfYear
    }
}

class Credit {
    constructor (name, desc, cl, minSum, rate, link, minInitSt, maxInitSt, spec, ndfl, mosprime, pur) {
        this.name = name
        this.desc = desc
        this.client = cl
        this.minSum = minSum
        this.rate = parseFloat(rate)
        this.link = link
        this.minInitSt = minInitSt
        this.maxInitSt = maxInitSt
        this.cur = ' <span class="rur">р<span>уб.</span></span>'
        this.spec = spec
        this.pay = 0
        this.pay2 = 0
        this.pay3 = 0
        this.ndfl = ndfl
        this.mosprime = mosprime
        this.pur = pur
    }
}

function stringTrim (s) {
    return s.toString().replace(/\s+|\s+/g, '')
}

function stringReverse (s) {
    return s.toString().split('').reverse().join('')
}

function startsWith (needle, s) {
    return s.toString().indexOf(needle) === 0
}

function trimStart (needle, s) {
    let startIndex = 0
    while (s.toString()[startIndex] === needle) {
        startIndex++
    }
    return s.toString().substr(startIndex)
}

function formatMoney (s) {
    const backward = stringReverse(stringTrim(s))
    const trimmed = stringReverse(backward.replace(/([0-9]{3})/g, '$1 '))
    return trimmed.replace(/\s+/g, ' ').trim()
}

function findClosest (number, array) {
    let current = array[0]
    let diff = Math.abs(number - current)
    for (let index = 0; index < array.length; index++) {
        const newdiff = Math.abs(number - array[index])
        if (newdiff < diff) {
            diff = newdiff
            current = array[index]
        }
    }
    return current
}

function createArraysForSlider (numIntervals) {
    let prefix = ''
    let currentValueStr = ''
    const realValues = []
    const sliderValues = []
    let intervalVal
    let currentValue
    let accuracy

    for (let i = 0; i < numIntervals.length - 1; i++) {
        currentValue = numIntervals[i]
        const firstValue = numIntervals[i]
        prefix = getPrefixForSum(currentValue)
        accuracy = getFactorForAccuracy(numIntervals[i].toString().length - 1)
        if (i === 0) {
            currentValue = accurRound(numIntervals[i], accuracy / 10, 1)
        }
        let difference = Math.abs(numIntervals[i + 1] - currentValue)
        const accuracyForDifference = getFactorForAccuracy(difference.toString().length - 1)
        difference = accurRound(difference, accuracyForDifference, 0)
        intervalVal = difference / 20
        realValues.push(+firstValue.toFixed(0))
        currentValueStr = getDigitNumber(currentValue.toString())
        sliderValues.push(`${currentValueStr} ${prefix}`)
        currentValue += intervalVal
        while (currentValue < numIntervals[i + 1]) {
            realValues.push(+currentValue.toFixed(0))
            sliderValues.push('')
            currentValue += intervalVal
        }
    }
    // console.log(realValues);

    realValues.push(numIntervals[numIntervals.length - 1])
    currentValueStr = getDigitNumber(numIntervals[numIntervals.length - 1].toString())
    currentValue = numIntervals[numIntervals.length - 1]
    prefix = getPrefixForSum(currentValue)
    sliderValues.push(`${currentValueStr} ${prefix}`)
    return {
        realValues,
        sliderValues,
        intervalVal
    }
}

function getPrefixForSum (value) {
    let prefix = ''
    if (value >= 1000 && value <= 999999) {
        prefix = 'тыс'
    } else if (value >= 1000000 && value <= 999999999) {
        prefix = 'млн'
    } else if (value >= 1000000000 && value <= 999999999999) {
        prefix = 'млдр'
    }

    return prefix
}

function getDigitNumber (value) {
    let valStr = ''
    let int = null
    switch (value.length % 3) {
        case 0:
            valStr = value.slice(0, 3)
            break
        case 1:
            valStr = value.slice(0, 2)
            int = (parseInt(valStr) / 10)
            if (int % 10 === 0) {
                int = int.toFixed(1)
            }
            valStr = int
            break
        case 2:
            valStr = value.slice(0, 2)
            break
    }
    return valStr
}
/**
 *
 * @param {Number} intervalsCount
 * @param {Number} min
 * @param {Number} max
 * @returns {Number[]}
 */
function createIntervals (intervalsCount, min, max) {
    min = parseInt(min.toFixed(0))
    max = parseInt(max.toFixed(0))

    const interval = []
    const step = Math.abs(max - min) / intervalsCount
    let initVal = min

    for (let i = 0; i <= intervalsCount; i++) {
        let roundDigit = roundDigits(initVal.toFixed(0), i, intervalsCount)
        if (i === 0) {
            roundDigit = initVal
        }
        interval.push(roundDigit)
        initVal += step
    }
    return interval
}

function roundDigits (value) {
    value = value.toString()
    const accuracyForValue = getFactorForAccuracy(value.length - 1)
    const roundVal = accurRound(value, accuracyForValue / 10, 1)
    return parseInt(roundVal)
}

function accurRound (value, accuracy, toFixed) {
    return parseInt((parseInt(value) / accuracy).toFixed(toFixed)) * accuracy
}

/**
 * @param {Number} value
 * @param {Number} accuracy
 * @param {Number} toFixed
 * @returns {Nubmer}
 */
function accuracyRound (value, accuracy, toFixed) {
    accuracy = parseInt(getFactorForAccuracy(accuracy))
    return (Math.round(value * accuracy) / accuracy).toFixed(toFixed)
}

function getFactorForAccuracy (count) {
    return parseInt('1' + '0'.repeat(count))
}

export {
    stringTrim,
    stringReverse,
    startsWith,
    trimStart,
    formatMoney,
    findClosest,
    createArraysForSlider,
    createIntervals,
    accuracyRound,
    Deposit,
    Credit
}
