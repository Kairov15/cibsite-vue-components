import Vue from 'vue'
import Vuex from 'vuex'
import {
    getField,
    updateField
} from 'vuex-map-fields'
import { model, objectTypes, testModel } from '../config/maternalData'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        model: { ...model },
        objectTypes,
        currentStep: 'Step10',
        currentStepIndex: 0,
        host: null
    },
    getters: {
        getField,
        isEmailNotification (state) {
            return state.model.notificationType === 'by.email'
        },
        isPhoneNotification (state) {
            return state.model.notificationType === 'by.phone'
        },
        isResidentialBuildingType (state) {
            const residentialBuildingType = ['residential_building', 'part_of_residential_building']
            return residentialBuildingType.includes(state.model.annexData.objectType)
        },
        Step10 (state) {
            return {
                loanContractNumber: state.model.loanContractNumber,
                loanContractDate: state.model.loanContractDate,
                officeId: state.model.officeId,
                fullName: state.model.fullName,
                personName: {
                    name: state.model.personName.name,
                    surname: state.model.personName.surname,
                    bornSurname: state.model.personName.bornSurname,
                    patronymic: state.model.personName.patronymic
                },
                applicantStatus: state.model.applicantStatus,
                snils: state.model.snils,
                certSeriesAndNumber: state.model.certSeriesAndNumber,
                certIssuedByAndDate: state.model.certIssuedByAndDate
            }
        },
        Step20 (state) {
            return {
                passport: {
                    passportData: state.model.passport.passportData,
                    issuedDate: state.model.passport.issuedDate,
                    divisionCode: state.model.passport.divisionCode,
                    issuedName: state.model.passport.issuedName
                },
                registrationAddress: {
                    postCode: state.model.registrationAddress.postCode,
                    region: state.model.registrationAddress.region,
                    district: state.model.registrationAddress.district,
                    locality: state.model.registrationAddress.locality,
                    street: state.model.registrationAddress.street,
                    houseNum: state.model.registrationAddress.houseNum,
                    flatNum: state.model.registrationAddress.flatNum
                },
                residenceAddress: {
                    postCode: state.model.residenceAddress.postCode,
                    region: state.model.residenceAddress.region,
                    district: state.model.residenceAddress.district,
                    locality: state.model.residenceAddress.locality,
                    street: state.model.residenceAddress.street,
                    houseNum: state.model.residenceAddress.houseNum,
                    flatNum: state.model.residenceAddress.flatNum
                },
                contactPhoneNumber: state.model.contactPhoneNumber
            }
        },
        Step30 (state) {
            return {
                typeOfExpenses: state.model.typeOfExpenses,
                capitalAmount: state.model.capitalAmount,
                notificationType: state.model.notificationType,
                email: state.model.email,
                codeWord: state.model.codeWord
            }
        },
        Step40 (state) {
            return {
                annexData: {
                    ownershipRightsDocumentData: state.model.annexData.ownershipRightsDocumentData,
                    cadastralNumber: state.model.annexData.cadastralNumber,
                    objectType: state.model.annexData.objectType,
                    objectAddress: {
                        region: state.model.annexData.objectAddress.region,
                        district: state.model.annexData.objectAddress.district,
                        locality: state.model.annexData.objectAddress.locality,
                        street: state.model.annexData.objectAddress.street,
                        houseNum: state.model.annexData.objectAddress.houseNum,
                        flatNum: state.model.annexData.objectAddress.flatNum
                    },
                    buildingRightsDocumentData: state.model.annexData.buildingRightsDocumentData,
                    steadRightsDocumentData: state.model.annexData.steadRightsDocumentData
                }
            }
        },
        Step50 (state) {
            return {
                annexData: {
                    steadCadastralNumber: state.model.annexData.steadCadastralNumber,
                    steadLandCategory: state.model.annexData.steadLandCategory,
                    steadArea: state.model.annexData.steadArea,
                    steadAddress: {
                        postCode: state.model.annexData.steadAddress.postCode,
                        region: state.model.annexData.steadAddress.region,
                        district: state.model.annexData.steadAddress.district,
                        locality: state.model.annexData.steadAddress.locality,
                        street: state.model.annexData.steadAddress.street,
                        houseNum: state.model.annexData.steadAddress.houseNum,
                        flatNum: state.model.annexData.steadAddress.flatNum
                    },
                    spouseFullName: state.model.annexData.spouseFullName
                }
            }
        }
    },
    mutations: {
        updateField
    },
    actions: {
    }
})
