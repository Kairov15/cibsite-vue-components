import Vue from 'vue'
import Vuex from 'vuex'
import {
    getField,
    updateField
} from 'vuex-map-fields'
import feedBackPageModel from '../config/feedBackPageModel'
import axios from 'axios'
import { uniqBy } from 'lodash'
Vue.use(Vuex)

export default new Vuex.Store({
    state: feedBackPageModel,
    getters: {
        getField,
        issetATMSAndTerminalsList (state) {
            return state.ATMSAndTerminalsList.length
        },
        currentDirectionOptions (state, getters) {
            let result = state.directionOptions.fiz
            if (getters.isLegal) {
                result = state.directionOptions.biz
            }
            return result
        },
        isNoClientFiz (state, getters) {
            return state.formData.notClient && !getters.isLegal
        },
        isClientFiz (state, getters) {
            return !state.formData.notClient && !getters.isLegal
        },
        isNoClientBiz (state, getters) {
            return state.formData.notClient && getters.isLegal
        },
        isClientBiz (state, getters) {
            return !state.formData.notClient && getters.isLegal
        },
        isStandartFizForm (state, getters) {
            return (getters.isClientFiz &&
                    (state.formData.direction === 'Информация по продуктам и услугам банка' ||
                    state.formData.direction === 'Жалоба/претензия' ||
                    state.formData.direction === 'Благодарность' ||
                    state.formData.direction === 'Предложения по улучшению качества обслуживания'
                    )
            )
        },
        isReportForm (state, getters) {
            return (getters.isClientFiz && (state.formData.direction === 'Жалоба/претензия'))
        },
        isStandartBizForm (state, getters) {
            return !state.formData.notClient && getters.isLegal
        },
        isDecreaseMortgageRatesForm (state, getters) {
            return (getters.isClientFiz && state.formData.direction === 'Снижение ставки по ипотеке')
        },
        isDecreaseMedicLoanForm (state, getters) {
            return (getters.isClientFiz && state.formData.direction === 'Снижение ставки медицинским работникам')
        },
        isCreditHolidaysFizForm (state, getters) {
            return (getters.isClientFiz && state.formData.direction === 'Кредитные каникулы')
        },
        isBankomansAndTerminalsForm (state, getters) {
            return (getters.isClientFiz && state.formData.direction === 'Банкоматы/терминалы самообслуживания')
        },
        isBankAppForm (state, getters) {
            return (getters.isClientFiz && state.formData.direction === 'Вопросы по работе сервисов Мобильный банк/Интернет-банк')
        },
        isScammersForm (state, getters) {
            return (getters.isClientFiz && state.formData.direction === 'Сообщить банку о подозрении на мошенничество')
        },
        isMobileApp (state, getters) {
            return (getters.isBankAppForm && state.formData.service === 'Приложение (Мобильный банк)')
        },
        isFullApp (state, getters) {
            return (getters.isBankAppForm && state.formData.service === 'Интернет-банк (полная версия с сайта)')
        },
        isCallBackContactMethod (state, getters) {
            return (getters.isScammersForm && state.formData.contactMethod === 'Поступил звонок')
        },
        isSiteContactMethod (state, getters) {
            return (getters.isScammersForm && state.formData.contactMethod === 'Увидел сайт')
        },
        isSmsContactMethod (state, getters) {
            return (getters.isScammersForm && state.formData.contactMethod === 'Получил SMS')
        },
        isEmailContactMethod (state, getters) {
            return (getters.isScammersForm && state.formData.contactMethod === 'Получил письмо по E-mail')
        },
        isSocialContactMethod (state, getters) {
            return (getters.isScammersForm && state.formData.contactMethod === 'Сообщение в соцсетях/мессенджерах')
        },
        isLegalInn (state) {
            return state.formData.inn ? state.formData.inn.length : 0
        },
        isLegal (state) {
            return state.formData.topic === 'Для юридических лиц/ИП'
        },
        isCreditHolidays (state) {
            return state.formData.direction === 'Кредитные каникулы'
        },
        isReduceMedicLoan (state) {
            return state.formData.direction === 'Снижение ставки медицинским работникам'
        },
        showTopicAndDirectionMod (state) {
            return state.showTopicAndDirection && !state.formData.notClient
        },
        isReduceFNSLoan (state) {
            return state.requestTypeParam === 'fns'
        },
        subdivisionText (state, getters) {
            let result = 'Подразделение банка'
            if (getters.isCreditHolidays || getters.isReduceMedicLoan || getters.isReduceFNSLoan || getters.isDecreaseMortgageRatesForm) {
                result = 'Подразделение банка, где вы получали кредит'
            }
            if (getters.isBankomansAndTerminalsForm) {
                result = 'Адрес банкомата/терминала'
            }
            return result
        },
        subtitle (state, getters) {
            let result = ''
            if (getters.isReduceMedicLoan) {
                result = 'Заявка на снижение ставки мед. работникам'
            }
            if (getters.isReduceFNSLoan) {
                result = 'Заявка на снижение ставки сотрудникам ФНС'
            }
            return result
        }
    },
    mutations: {
        updateField,
        setOffices (state, offices) {
            state.officesList = offices
        },
        togglePreloader (state, value) {
            state.showLoader = value
        },
        setATMSAndTerminalsList (state, ATMSAndTerminalsList) {
            state.ATMSAndTerminalsList = ATMSAndTerminalsList
        }
    },
    actions: {
        getOfficesList ({ commit }, payload) {
            const url = `${payload.host}cabinet/ajax/yandexmapcoordinate2/?call=getFullOffices${payload.params}`
            commit('togglePreloader', true)
            let offices = []
            axios.get(url)
                .then((res) => {
                    offices = res.data.map((item) => ({
                        value: `${item.city_title}, ${item.text}`,
                        id: item.id
                    }))
                    commit('setOffices', offices)
                })
                .catch(error => {
                    console.log(error)
                })
                .finally(() => {
                    setTimeout(() => {
                        commit('togglePreloader', false)
                    }, 500)
                })
        },
        async getATMSAndTerminalsList ({ commit, getters }) {
            if (!getters.issetATMSAndTerminalsList) {
                commit('togglePreloader', true)
                try {
                    const [terminalsResponse, ATMSResponse] = await Promise.all([
                        axios.post('https://site-api.centrinvest.ru/api/plugin/terminal/v2/terminal/list'),
                        axios.post('https://site-api.centrinvest.ru/api/plugin/atm/v2/atm/list')
                    ])
                    await new Promise(resolve => {
                        let terminalsList = terminalsResponse.data.data
                        let ATMSList = ATMSResponse.data.data
                        let result = []
                        terminalsList = uniqBy(terminalsList, (l) => l.address)
                        ATMSList = uniqBy(ATMSList, (l) => l.address_full)
                        ATMSList = ATMSList.map(atm => {
                            return {
                                ...atm,
                                address: atm.address_full
                            }
                        })
                        result = [...terminalsList, ...ATMSList]
                        result = uniqBy(result, (l) => l.address)
                        result = result.map((item) => {
                            return {
                                address: `${item.address.replace(/[0-9]{6},/ig, '')} (${item.facility_name})`,
                                id: item.id
                            }
                        })
                        commit('setATMSAndTerminalsList', result)
                        resolve()
                    })
                } catch (error) {
                    if (error.response) {
                        console.log(error.response.data)
                        console.log(error.response.status)
                        console.log(error.response.headers)
                    } else if (error.request) {
                        console.log(error.request)
                    } else {
                        console.log('Error', error.message)
                    }
                    console.log(error.config)
                } finally {
                    commit('togglePreloader', false)
                }
            }
        }
    }
})
