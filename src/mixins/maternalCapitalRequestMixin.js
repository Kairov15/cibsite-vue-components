import { mapFields } from 'vuex-map-fields'
import errorMessages from '../services/errorMessages'

const maternalCapitalRequestMixin = {
    components: {
        cibInput: () => import(/* webpackChunkName: "cibInput" */'../appComponents/cibInput'),
        vSelect: () => import(/* webpackChunkName: "vueSelect" */'vue-select'),
        message: () => import(/* webpackChunkName: "popupMessage" */'../appComponents/popupMessage')
    },
    props: {
        $v: {
            type: Object,
            default: () => ({})
        }
    },
    data: () => ({
        errorMessages
    }),
    computed: {
        ...mapFields([
            'model',
            'host',
            'currentStepIndex'
        ]),
        disabledDates () {
            return {
                issuedDate: {
                    from: new Date()
                }
            }
        },
    },
    methods: {
        findSuggestionsArr (fieldName) {
            let result = this.suggestionsArrByFieldName.find(item => {
                return item.fieldName === fieldName
            })
            result = result ? result.suggestions : []
            return result
        },
        findIndexByFieldName (suggestionData) {
            return this.suggestionsArrByFieldName.findIndex(suggestion => {
                return suggestion.fieldName === suggestionData.fieldName
            })
        },
    }
}

export default maternalCapitalRequestMixin
