import { mapFields } from 'vuex-map-fields'

const fieldGroupMixin = {
    props: {
        $v: {
            type: Object,
            default: () => ({})
        },
        attrs: {
            type: Object,
            default: () => ({})
        }
    },
    components: {
        cibInput: () => import(/* webpackChunkName: "cibInput" */'../../appComponents/cibInput'),
    },
    computed: {
        ...mapFields([
            'formData'
        ])
    }
}

export default fieldGroupMixin
