
const fieldGroupMixin = {
    props: {
        $v: {
            type: Object,
            default: () => ({})
        }
    },
    components: {
        cibInput: () => import(/* webpackChunkName: "cibInput" */'../../appComponents/cibInput'),
        cibLink: () => import(/* webpackChunkName: "cibLink" */'../../appComponents/link'),
        cibButton: () => import(/* webpackChunkName: "cibButton" */'../../appComponents/Button'),
    }
}

export default fieldGroupMixin
