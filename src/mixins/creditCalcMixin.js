import { findClosest, createArraysForSlider, createIntervals } from '../utils/common'
const creditCalcMixin = {
    props: {
        programms: {
            type: Array
        },
        sliderDataProp: {
            type: Object
        },
        incomeParams: {
            type: Object
        },
        region: {
            type: String
        }
    },
    components: {
        cibInput: () => import(/* webpackChunkName: "cibInput" */'../appComponents/cibInput'),
        vueSlider: () => import(/* webpackChunkName: "vueSlider" */'../appComponents/components/vue-slider')
    },
    created () {
        this.init()
    },
    data () {
        return {
            changeSumFromSlider: false,
            changePrepayFromSlider: false,
            sliderData: this.sliderDataProp,
            prepaySliderRealValues: [],
            prepaySliderLabels: [],
            params: {
                sumSliderValue: null,
                prepaySliderValue: null,
                client: true,
                ndfl2: true,
                sum: null,
                med: false,
                prepay: null,
                accelerator: false,
                period: 1,
                ruralAreas: false,
                mortgageTypes: this.incomeParams.mortgageTypes,
                showProgrammsList: this.incomeParams.showProgrammsList,
                showMedCheckbox: this.incomeParams.showMedCheckbox,
                showAccCheckbox: this.incomeParams.showAccCheckbox,
            }
        }
    },
    computed: {
        sliderRealValues () {
            return this.sliderData.sliderRealValues
        },
        sliderLabels () {
            return this.sliderData.sliderLabels
        },
        sumSliderMax () {
            return (this.sliderData.sliderRealValues.length - 1)
        },
        prepaySliderMax () {
            return (this.prepaySliderRealValues.length - 1)
        },
        periodSliderMax () {
            return this.programms.reduce((max, nextProgramm) => {
                const { monthCalculusPeriod } = nextProgramm
                const maxrate = nextProgramm.rates.reduce((maxrate, currentRate) => {
                    const maxPeriod = monthCalculusPeriod ? currentRate.maxPeriod / 12 : currentRate.maxPeriod
                    return Math.max(maxrate, maxPeriod)
                }, 0)
                return Math.max(max, maxrate)
            }, 0)
        },
        prepayValueBoundary () {
            let max, min
            min = Math.round(this.params.sum * this.initPrepaymentPercentage)
            max = Math.round((min >= this.params.sum * 0.95) && this.sliderData.intervals ? this.params.sum - createArraysForSlider(this.sliderData.intervals).intervalVal : this.params.sum * 0.95)
            return { max, min }
        },
        initPrepaymentPercentage () {
            return this.programms.reduce((min, nextProgramm) => {
                const minInstalment = nextProgramm.rates.reduce((minInstalment, currentInstalment) => {
                    return Math.min(minInstalment, currentInstalment.minInstalment)
                }, 100)
                return Math.min(min, minInstalment)
            }, 100) / 100
        },
        creditSum () {
            return this.params.prepay ? this.params.sum - this.params.prepay : this.params.sum
        },
        num () {
            return this.params.period * 12
        },
        prepaymentPercentage () {
            const result = 100 * this.params.prepay / this.params.sum
            // return Math.round(result.toFixed(2))
            return result
        }
    },
    methods: {
        init () {
            return new Promise((resolve, reject) => {
                this.createSliderValues()
                this.params.sum = this.sliderRealValues[0]
                this.params.prepay = this.prepayValueBoundary.min
                this.params = Object.assign(this.params, this.incomeParams)
                resolve()
            })
        },
        sliderValueChanged (name) {
            this[name] = true
        },
        createSliderValues () {
            if (this.sliderData.intervals) {
                this.sliderData.sliderRealValues = createArraysForSlider(this.sliderData.intervals).realValues
                this.sliderData.sliderLabels = createArraysForSlider(this.sliderData.intervals).sliderValues
            }
        },
        createPrepaySliderValues (min = this.prepayValueBoundary.min, max = this.prepayValueBoundary.max) {
            const intervals = createIntervals(3, Math.round(min), Math.round(max))
            this.prepaySliderRealValues = createArraysForSlider(intervals).realValues
            this.prepaySliderLabels = createArraysForSlider(intervals).sliderValues
        },
        changePrepay (min = this.prepayValueBoundary.min, max = this.prepayValueBoundary.max) {
            if (this.params.prepay <= min) {
                this.params.prepay = Math.round(min)
            } else if (this.params.prepay >= max) {
                this.params.prepay = Math.round(max)
            }
            this.params.prepaySliderValue = this.prepaySliderRealValues.indexOf(findClosest(this.params.prepay, this.prepaySliderRealValues))
        },
        toggleHint (value) {
            this.$emit('setHint', value)
        },
        isDesiredRegion (region) {
            let reg = region || 'all'
            let isDesiredRegion = reg === 'all'
            if (reg !== 'all') {
                const isNegation = reg.match(/!/i)
                reg = reg.replace('!', '')
                let regArr = reg.split(',')
                regArr = regArr.map((value) => {
                    return value.trim()
                })
                isDesiredRegion = isNegation ? !regArr.includes(this.region) : regArr.includes(this.region)
            }
            return isDesiredRegion
        }
    },
    watch: {
        'params.sum' () {
            this.changeSumFromSlider = false
            this.params.sumSliderValue = this.sliderRealValues.indexOf(findClosest(this.params.sum, this.sliderRealValues))
            this.createPrepaySliderValues()
            this.changePrepay()
        },
        'params.prepay' () {
            this.changePrepayFromSlider = false
            this.params.prepaySliderValue = this.prepaySliderRealValues.indexOf(findClosest(this.params.prepay, this.prepaySliderRealValues))
        },
        'params.sumSliderValue' () {
            if (this.changeSumFromSlider) {
                this.params.sum = this.sliderRealValues[this.params.sumSliderValue]
            }
        },
        'params.prepaySliderValue' () {
            if (this.changePrepayFromSlider) {
                this.params.prepay = Math.round(this.prepaySliderRealValues[this.params.prepaySliderValue])
            }
        },
        finalProgramms: {
            handler () {
                this.$emit('calculationsCompleted', {
                    finalProgramms: this.finalProgramms,
                    paramsFromComponent: {
                        sum: this.params.sum,
                        creditSum: this.creditSum,
                        period: this.params.period,
                        prepay: this.params.prepay,
                        initPrepaymentPercentage: this.initPrepaymentPercentage * 100,
                        client: this.params.client,
                        ndfl2: this.params.ndfl2,
                        ruralAreas: this.params.ruralAreas
                    }
                })
            },
            deep: true
        },
        errors: {
            handler () {
                this.$emit('onError', this.errors)
            },
            deep: true
        }
    }
}

export default creditCalcMixin
