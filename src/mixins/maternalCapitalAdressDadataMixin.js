import { fiasIdObject } from '../config/maternalData'

const maternalCapitalAdressDadataMixin = {
    data: () => ({
        dadataMeta: {
            residenceAddress: fiasIdObject
        },
        suggestionsArrByFieldName: [],
        fiasIdObject: {},
        dadataTypesObject: {},
        locality: null
    }),
    computed: {
        regionDadataQueryParams () {
            return {
                from_bound: { value: 'region' },
                to_bound: { value: 'region' }
            }
        },
        disabledDates () {
            return {
                issuedDate: {
                    from: new Date()
                }
            }
        },
        districtDadataQueryParams () {
            const residenceAddress = this.dadataMeta.residenceAddress
            return {
                locations: [
                    {
                        region_fias_id: residenceAddress.region_fias_id
                    }
                ],
                from_bound: {
                    value: 'area'
                },
                to_bound: {
                    value: 'area'
                },
                restrict_value: true
            }
        },
        localityDadataQueryParams () {
            const residenceAddress = this.dadataMeta.residenceAddress
            const fiasId = residenceAddress.area_fias_id || residenceAddress.region_fias_id
            const fiasIdKey = this.getKeyByValue(residenceAddress, fiasId) || 'region_fias_id'
            return {
                locations: [
                    {
                        [fiasIdKey]: fiasId
                    }
                ],
                from_bound: {
                    value: 'city'
                },
                to_bound: {
                    value: 'settlement'
                },
                restrict_value: true
            }
        },
        streetDadataQueryParams () {
            const residenceAddress = this.dadataMeta.residenceAddress
            const fiasId = residenceAddress.settlement_fias_id || residenceAddress.city_fias_id || null
            const fiasIdKey = this.getKeyByValue(residenceAddress, fiasId) || 'settlement_fias_id'
            return {
                locations: [
                    {
                        [fiasIdKey]: fiasId
                    }
                ],
                from_bound: {
                    value: 'street'
                },
                to_bound: {
                    value: 'street'
                },
                restrict_value: true
            }
        },
        houseNumDadataQueryParams () {
            const residenceAddress = this.dadataMeta.residenceAddress
            const fiasId = residenceAddress.street_fias_id || residenceAddress.settlement_fias_id || null
            const fiasIdKey = this.getKeyByValue(residenceAddress, fiasId) || 'street_fias_id'
            return {
                locations: [
                    {
                        [fiasIdKey]: fiasId
                    }
                ],
                from_bound: {
                    value: 'house'
                },
                restrict_value: true
            }
        }
    },
    methods: {
        findIndexByFieldName (suggestionData) {
            return this.suggestionsArrByFieldName.findIndex(suggestion => {
                return suggestion.fieldName === suggestionData.fieldName
            })
        },
        suggestHandler (suggestionData) {
            const index = this.findIndexByFieldName(suggestionData)
            if (index === -1) {
                this.suggestionsArrByFieldName.push(suggestionData)
            } else {
                this.suggestionsArrByFieldName[index].suggestions = suggestionData.suggestions
            }
            this.showSuggestion[suggestionData.fieldName] = true
        },
        onHintClick (valueTypesObj, suggestionData, fillMethodName) {
            this.locality = null
            this.fiasIdObject = {
                region_fias_id: suggestionData.data.region_fias_id,
                area_fias_id: suggestionData.data.area_fias_id,
                city_fias_id: suggestionData.data.city_fias_id,
                settlement_fias_id: suggestionData.data.settlement_fias_id,
                street_fias_id: suggestionData.data.street_fias_id,
                house_fias_id: suggestionData.data.house_fias_id
            }
            this.dadataTypesObject = {
                region_with_type: suggestionData.data.region_with_type,
                area_with_type: suggestionData.data.area_with_type,
                city_with_type: suggestionData.data.city_with_type,
                settlement_with_type: suggestionData.data.settlement_with_type,
                street_with_type: suggestionData.data.street_with_type,
                house_with_type: suggestionData.data.house_with_type,
                ...valueTypesObj
            }
            this.dadataMeta.residenceAddress = this.fiasIdObject
            if (this.dadataTypesObject.settlement_with_type && !this.dadataTypesObject.city_with_type) {
                this.locality = this.dadataTypesObject.settlement_with_type
            } else if (!this.dadataTypesObject.settlement_with_type && this.dadataTypesObject.city_with_type) {
                this.locality = this.dadataTypesObject.city_with_type
            } else if (this.dadataTypesObject.settlement_with_type && this.dadataTypesObject.city_with_type) {
                this.locality = `${this.dadataTypesObject.city_with_type}, ${this.dadataTypesObject.settlement_with_type}`
            }
            this[fillMethodName]()
            for (const key in this.showSuggestion) {
                this.showSuggestion[key] = false
            }
        },
        getKeyByValue (object, value) {
            return Object.keys(object).find(key => object[key] === value)
        },
        findSuggestionsArr (fieldName) {
            let result = this.suggestionsArrByFieldName.find(item => {
                return item.fieldName === fieldName
            })
            result = result ? result.suggestions : []
            return result
        }
    }
}

export default maternalCapitalAdressDadataMixin
