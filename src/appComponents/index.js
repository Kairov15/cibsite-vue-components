import AcquiringCalc from './AcquiringCalc'
import CommentPageForm from './CommentPageForm'
import CreditCalc from './CreditCalc'
import DepositCalc from './DepositCalc'
import FeedbackForm from './FeedbackForm'
import FeedBackPage from './FeedBackPage'
import IncomeReductionCalc from './IncomeReductionCalc'
import NewsCalendar from './NewsCalendar'
import FeedBackWidget from './FeedBackWidget'
import Vote from './Vote'
import MaternalCapitalRequest from './MaternalCapitalRequest'

export {
    AcquiringCalc,
    CommentPageForm,
    CreditCalc,
    DepositCalc,
    FeedbackForm,
    FeedBackPage,
    IncomeReductionCalc,
    NewsCalendar,
    FeedBackWidget,
    Vote,
    MaternalCapitalRequest
}
