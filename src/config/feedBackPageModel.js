export default {
    showTopicAndDirection: true,
    formData: {
        organization: null,
        notClient: false, // new
        email: null,
        fio: null,
        phone: null,
        callBackPhone: null, // new
        contactMethod: null, // new
        fakeSiteAddress: null, // new
        fakeEmail: null, // new
        socialNetworkOrMessenger: null, // new
        profileLinkOrPhone: null, // new
        service: null, // new
        phoneModel: null, // new
        phoneOS: null, // new
        appVersion: null, // new
        PCOS: null, // new
        browserName: null, // new
        dateAndTime: null, // new
        topic: null,
        inn: null,
        account: null,
        direction: null,
        passportData: null,
        creditDate: null,
        vacationPeriod: 1,
        officeId: null,
        ATMOrTerminalAddress: null, // new
        message: null,
        feedBackAfterCheck: true,
        agreement: false,
        files: [],
        loanNumber: null
    },
    suggestionsFromComponent: [],
    ATMSAndTerminalsList: [],
    showSuggestion: {
        organizationField: false,
        innField: false
    },
    topicOptions: [
        'Для физических лиц',
        'Для юридических лиц/ИП'
    ],
    contactMethodOptions: [
        'Поступил звонок',
        'Увидел сайт',
        'Получил SMS',
        'Получил письмо по E-mail',
        'Сообщение в соцсетях/мессенджерах'
    ],
    serviceOptions: [
        'Приложение (Мобильный банк)',
        'Интернет-банк (полная версия с сайта)'
    ],
    socialsOptions: [
        'Вконтакте',
        'Одноклассники',
        'WhatsApp',
        'Viber',
        'Instagram',
        'Другое'
    ],
    directionOptions: {
        fiz: [
            'Информация по продуктам и услугам банка',
            'Снижение ставки по ипотеке',
            'Снижение ставки медицинским работникам',
            'Кредитные каникулы',
            'Вопросы по работе сервисов Мобильный банк/Интернет-банк',
            'Банкоматы/терминалы самообслуживания',
            'Жалоба/претензия',
            'Благодарность',
            'Сообщить банку о подозрении на мошенничество',
            'Предложения по улучшению качества обслуживания'
        ],
        biz: [
            'Вопросы по услугам для ЮЛ/ИП',
            'ДБО (Банк-Клиент)',
            'Кредитные каникулы',
            'Кредитование ЮЛ/ИП',
            'Жалоба/претензия',
            'Благодарность',
            'Предложения по улучшению качества обслуживания'
        ]
    },
    availableExtentions: [
        'zip', 'rar', '7z', 'txt', 'rtf', 'doc', 'docx', 'xls', 'xlsx', 'ods', 'odt', 'jpg', 'jpeg', 'bmp', 'png', 'pdf'
    ],
    availableSize: 15728640,
    uploadFileErrors: [],
    showLoader: false,
    officesList: [],
    isProduction: process.env.NODE_ENV === 'production',
    showModal: false,
    requestNumber: null,
    sendErrors: null,
    requestTypeParam: null,
    hostParam: null
}
