export const categories = {
    fastFood: {
        label: 'Предприятия быстрого питания',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,29%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,79%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,49%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,20%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '1,70%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,40%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,30%'
                    }
                ]
            }
        }
    },
    productsShop: {
        label: 'Продуктовые магазины и супермаркеты',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,29%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,99%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,89%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,20%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '1,90%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,80%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,60%'
                    }
                ]
            }
        }
    },
    gasStations: {
        label: 'Автозаправочные станции',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,29%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,99%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,89%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,20%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '1,90%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,80%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,60%'
                    }
                ]
            }
        }
    },
    autoServices: {
        label: 'Продажа, лизинг и обслуживание автомобилей',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,29%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,89%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,69%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,20%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '1,80%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,60%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,50%'
                    }
                ]
            }
        }
    },
    clothingStore: {
        label: 'Магазины одежды',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,29%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,89%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '2,19%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,30%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,20%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,10%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,90%'
                    }
                ]
            }
        }
    },
    constructionTimber: {
        label: 'Строительные лесоматериалы',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,39%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,29%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '2,19%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,30%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,20%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,10%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,90%'
                    }
                ]
            }
        }
    },
    carsPart: {
        label: 'Автомобильные запчасти и аксессуары',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,39%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,29%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '2,19%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,30%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,20%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,10%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,90%'
                    }
                ]
            }
        }
    },
    furnitureStore: {
        label: 'Мебельные магазины',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,39%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,29%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '2,19%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,30%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,20%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,10%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,90%'
                    }
                ]
            }
        }
    },
    utilities: {
        label: 'Коммунальные услуги (СНТ, ТСЖ)',
        equipment: {
            free: {
                rev: [{
                    minSum: 0,
                    maxSum: 2147483647,
                    value: '1,8%'
                }]
            },
            buy: {
                rev: [{
                    minSum: 0,
                    maxSum: 2147483647,
                    value: '1,7%'
                }]
            }
        }
    },
    hospitals: {
        label: 'Больницы',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,00%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '1,99%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '1,69%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,59%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,49%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '1,90%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '1,89%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '1,59%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,49%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,39%'
                    }
                ]
            }
        }
    },
    pharmacies: {
        label: 'Аптеки',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,50%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,29%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,99%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,89%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,39%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,19%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,89%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,79%'
                    }
                ]
            }
        }
    },
    educational: {
        label: 'Образовательные курсы и кружки',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,50%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,49%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,29%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,99%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,89%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,39%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,19%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '1,89%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '1,79%'
                    }
                ]
            }
        }
    },
    other: {
        label: 'Другое',
        equipment: {
            free: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,5% + 700 рублей'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,5%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,5%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,5%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '2,5%'
                    }
                ]
            },
            buy: {
                rev: [{
                        minSum: 0,
                        maxSum: 99999,
                        value: '2,40%'
                    },
                    {
                        minSum: 100000,
                        maxSum: 249999,
                        value: '2,40%'
                    },
                    {
                        minSum: 250000,
                        maxSum: 499999,
                        value: '2,40%'
                    },
                    {
                        minSum: 500000,
                        maxSum: 999999,
                        value: '2,40%'
                    },
                    {
                        minSum: 1000000,
                        maxSum: 2147483647,
                        value: '2,40%'
                    }
                ]
            }
        }
    }
}
