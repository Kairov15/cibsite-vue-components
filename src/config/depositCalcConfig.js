const url = '/deposits.json'
let printUrl = ''
if (process.env.NODE_ENV === 'production') {
    printUrl = '/deposit-calc/print-deposit'
}

export const SliderDataProvider = {
    depositSumIntervals: [10000, 100000, 350000, 500000, 700000, 1000000, 1300000, 5000000, 10000000],
    depositSumSliderData: [],
    depositSumRealValues: [],

    depositSumIntervalsCur: [500, 5000, 10000, 200000, 500000, 1000000],
    depositSumSliderDataCur: [],
    depositSumRealValuesCur: [],

    termPlaceIntervals: {
        '1-MONTH': '1 мес',
        '3-MONTH': '3 мес',
        '6-MONTH': '6 мес',
        '9-MONTH': '9 мес',
        '12-MONTH': '12 мес',
        '17-MONTH': '17 мес',
        '18-MONTH': '1,5 года',
        '24-MONTH': '2 года',
        '36-MONTH': '3 года'
    },
    termPlaceSumSliderData: [],
    termPlaceSumRealValues: []
}

export const appConfig = {
    url: url,
    deposits: '',
    id: '#calc',
    creditsList: [],
    counting: true,
    changeSumFromSlider: true,
    sumSliderData: SliderDataProvider.depositSumSliderData,
    sumRealValues: SliderDataProvider.depositSumRealValues,
    sumSliderValue: 9,
    printLink: printUrl,
    printTrigged: false,
    region: null,
    rightTime: 1,
    termPlaceSelect: {
        '1-MONTH': '1 месяц',
        '3-MONTH': '3 месяца',
        '6-MONTH': '6 месяцев',
        '9-MONTH': '9 месяцев',
        '12-MONTH': '1 год',
        '18-MONTH': '1,5 года',
        '24-MONTH': '2 года',
        '36-MONTH': '3 года'
    },
    checkBoxes: {
        repl: 'Пополнение',
        part: 'Частичное снятие',
        pens: 'Пенсионный',
        cap: 'Капитализация процентов'
    },
    periodPaymentSelect: {
        ALL: 'Любая',
        MONTHLY: 'Ежемесячно',
        QUARTERLY: 'Ежеквартально',
        SEMIANNUALY: 'Раз в полгода',
        ANNUALY: 'Ежегодно'
    },
    currencyRadio: {
        RUB: `<svg width="19" height="22" viewBox="0 0 19 22" class="rub-symbol" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11.1914 17.9717H6.28418V22H3.48633V17.9717H0.454102V15.6572H3.48633V13.6504H0.454102V11.3506H3.48633V0.671875H11.3525C13.6377 0.671875 15.4492 1.25293 16.7871 2.41504C18.125 3.57715 18.7939 5.14941 18.7939 7.13184C18.7939 9.21191 18.1494 10.8184 16.8604 11.9512C15.5811 13.0742 13.7598 13.6406 11.3965 13.6504H6.28418V15.6572H11.1914V17.9717ZM6.28418 11.3506H11.3525C12.8662 11.3506 14.0186 10.9941 14.8096 10.2812C15.6006 9.56836 15.9961 8.52832 15.9961 7.16113C15.9961 5.9209 15.5859 4.91992 14.7656 4.1582C13.9453 3.38672 12.8369 2.99609 11.4404 2.98633H6.28418V11.3506Z"/>
        </svg>
        `
        // 'EUR': '€',
        // 'USD': '$'
    },
    stateParams: {
        sum: 1300000,
        cur: 'RUB',
        termPlace: '12-MONTH',
        cap: false,
        repl: false,
        part: false,
        pens: false,
        periodPayment: 'ALL',
        print: false
    },
    tableName: {
        name: {
            name: 'Наименование вклада',
            hint: ''
        },
        proc: {
            name: 'Ставка',
            hint: 'Годовая процентная ставка, указанная в договоре (или без учета капитализации процентов).'
        },
        yield: {
            name: 'Доходность',
            hint: 'Фактическая доходность - ставка с учетом капитализации (в процентах годовых), рассчитывается от начала срока действия договора вклада до конца каждого периода, указывается справочно.'
        },
        profit: {
            name: 'Сумма процентов',
            hint: 'Калькулятор позволяет рассчитать примерную сумму процентов.'
        }
    }
}
