const testModel = {
    loanContractNumber: '00156323',
    loanContractDate: '12.10.2015',
    officeId: 'd9fe21ff-a513-11e7-b635-628caf01d019',
    fullName: null,
    personName: {
        name: 'Елена',
        surname: 'Иванова',
        bornSurname: 'Лексова',
        patronymic: 'Анатольевна'
    },
    applicantStatus: 'mother',
    snils: '121-301-654-12',
    certSeriesAndNumber: 'МК-Э-071-2020 No1123545',
    certIssuedByAndDate: 'Государственное учреждение – Управление Пенсионного фонда Российской Федерации в г. Ростове-на-Дону (межрайонное) 10 февраля 2021г.',
    passport: {
        passportData: '6003 123456',
        issuedDate: '06.05.2003',
        divisionCode: '610-015',
        issuedName: 'ГУ МВД России по Ростовской области'
    },
    residenceAddressSameAsRegistration: false,
    registrationAddress: {
        postCode: '344019',
        region: 'Ростовская область',
        district: null,
        locality: 'город Ростов-на-Дону',
        street: 'улица Каяни',
        houseNum: 'д. 35',
        flatNum: null
    },
    residenceAddress: {
        postCode: null,
        region: null,
        district: null,
        locality: null,
        street: null,
        houseNum: null,
        flatNum: null
    },
    contactPhoneNumber: '+7 (928) 146-22-53',
    email: 'a.sasin@cinet.ru',
    typeOfExpenses: 'initial_fee.purchase',
    capitalAmount: 639431.83,
    notificationType: 'by.phone',
    codeWord: 'realmadrid',
    annexData: {
        ownershipRightsDocumentData: 'Выписка из ЕГРН от 15.10.2018г.',
        cadastralNumber: '61:02:0010205:6255',
        objectType: 'residential_building',
        objectAddress: {
            region: 'Ростовская область',
            district: null,
            locality: 'город Ростов-на-Дону',
            street: 'улица Каяни',
            houseNum: '35',
            flatNum: null
        },
        buildingRightsDocumentData: 'Документ, подтверждающий право на проведение работ по строительству (реконструкции) объекта ',
        steadRightsDocumentData: 'Свидетельство о государственной регистрации права серия 61-АЗ номер 503045 от 15.10.2015г.',
        steadCadastralNumber: '61:02:0010205:6354',
        steadLandCategory: 'земли населенных пунктов',
        steadArea: '254',
        steadAddress: {
            region: 'Ростовская область',
            district: null,
            locality: 'город Ростов-на-Дону',
            street: 'улица Каяни',
            houseNum: '35',
            flatNum: null
        },
        spouseFullName: 'Иванов Алексей Викторович'
    }
}

const model = {
    loanContractNumber: null,
    loanContractDate: null,
    officeId: null,
    fullName: null,
    personName: {
        name: null,
        surname: null,
        bornSurname: null,
        patronymic: null
    },
    applicantStatus: 'mother',
    snils: null,
    certSeriesAndNumber: null,
    certIssuedByAndDate: null,
    passport: {
        passportData: null,
        issuedDate: null,
        divisionCode: null,
        issuedName: null
    },
    residenceAddressSameAsRegistration: false,
    registrationAddress: {
        postCode: null,
        region: null,
        district: null,
        locality: null,
        street: null,
        houseNum: null,
        flatNum: null
    },
    residenceAddress: {
        postCode: null,
        region: null,
        district: null,
        locality: null,
        street: null,
        houseNum: null,
        flatNum: null
    },
    contactPhoneNumber: null,
    typeOfExpenses: null,
    capitalAmount: null,
    notificationType: null,
    email: null,
    codeWord: null,
    annexData: {
        ownershipRightsDocumentData: null,
            cadastralNumber: null,
            objectType: null,
            objectAddress: {
            region: null,
                district: null,
                locality: null,
                street: null,
                houseNum: null,
                flatNum: null
        },
        buildingRightsDocumentData: null,
        steadRightsDocumentData: null,
        steadCadastralNumber: null,
        steadLandCategory: null,
        steadArea: null,
        steadAddress: {
        region: null,
            district: null,
            locality: null,
            street: null,
            houseNum: null,
            flatNum: null
        },
        spouseFullName: null
    }
}
const fiasIdObject = {
    region_fias_id: null,
    area_fias_id: null,
    city_fias_id: null,
    settlement_fias_id: null,
    street_fias_id: null,
    house_fias_id: null
}

const applicantStatuses = {
    father: 'Отец',
    mother: 'Мать',
    child: 'Ребенок'
}
const typesOfExpenses = {
    'initial_fee.purchase': 'Уплата первоначального взноса при получении кредита на приобретение жилья',
    'initial_fee.build': 'Уплата первоначального взноса при получении кредита на строительство жилья',
    'principal_repayment_percent.purchase': 'Погашение основного долга и уплату процентов по кредиту на приобретение жилья',
    'principal_repayment_percent.build': 'Погашение основного долга и процентов по кредиту на строительство жилья',
    'principal_repayment_percent.refinance.purchase': 'Погашение основного долга и уплату процентов по рефинансированному кредиту на строительство жилья',
    'principal_repayment_percent.refinance.build': 'Погашение основного долга и уплату процентов по рефинансированному кредиту на приобретение жилья'
}

const objectTypes = {
    residential_building: 'Жилой дом',
    part_of_residential_building: 'Часть жилого дома',
    flat: 'Квартира',
    part_of_flat: 'Часть квартиры',
    room: 'Комната'
}

const notificationTypes = [
    {
        value: 'by.email',
        text: 'На адрес электронной почты'
    },
    {
        value: 'by.phone',
        text: 'СМС сообщением на контактный номер телефона'
    }
]
const dadataTypesObject = {
    region_with_type: null,
    area_with_type: null,
    city_with_type: null,
    settlement_with_type: null,
    street_with_type: null,
    house_with_type: null
}
export { model, testModel, fiasIdObject, applicantStatuses, typesOfExpenses, notificationTypes, dadataTypesObject, objectTypes }
