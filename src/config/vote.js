export default [
    {
        name: 'Фомина Юлия',
        slogan: 'Стремлюсь познать все аспекты работы в банке',
        post: 'Филиальная сеть, в штате банка с июня 2020 г.<br>контролер-кассир операционной кассы, г. Нижний Новгород',
        img: require('../assets/img/fomina.jpg'),
        voteResult: 'fomina',
        achievements: [
            'Несмотря на короткий период работы в банке, уже умеет полноценно замещать зав. кассой;',
            'Активно помогает в обработке заявок физических лиц и консультациях по кредитованию.'
        ]
    },
    {
        name: 'Кушнаренко Дарья',
        slogan: 'Вызывать любовь к кредитованию с первого взгляда, воплощая мечты людей в реальность',
        post: 'Филиальная сеть, в штате банка с февраля 2019 г.<br>экономист кредитного отдела, г. Краснодар',
        img: require('../assets/img/kushnarenko.jpg'),
        voteResult: 'kushnarenko',
        achievements: [
            'Полный спектр работы кредитования физических лиц;',
            'В период внештатной деятельности одна из первых, кто освоила и предложила эффективную методику обработки заявок партнеров Краснодарского края;',
            'Передает свой личный опыт молодым стажерам, которые, с горящими глазами, называют Дарью «Наш лучший Наставник!!».'
        ]
    },
    {
        name: 'Абрамян Маргарита',
        slogan: 'Даже самый крупный успех завершается поиском новых направлений для новых преобразований!',
        post: 'Управление розничных продуктов, в штате банка с июля 2020 г.<br>экономист отдела клиентского обслуживания',
        img: require('../assets/img/abramyan.jpg'),
        voteResult: 'abramyan',
        achievements: [
            'Отличается высокой производительностью труда;',
            'За 4 месяца работы в штате КПД Маргариты на 38% больше показателей сотрудника этого же участка за предыдущие 6 мес;',
            'Обрабатывает каждую заявку на выпуск карты, полученную дистанционным путем;',
            'При высоких показателях в работе также добивается больших успехов в учебе.'
        ]
    },
    {
        name: 'Ишмакова Эльмира',
        slogan: 'Кто хочет — ищет возможности, а кто не хочет — ищет причины',
        post: 'ОПЕРУ, в штате банка с декабря 2020 г.<br>экономист отдела документарной обработки документов',
        img: require('../assets/img/ishmakova.jpg'),
        voteResult: 'ishmakova',
        achievements: [
            'Способность слушать клиентов, повышать свой уровень знаний, а также умение работать в команде ТОТ &ndash; это одни из тех качеств, которыми должен обладать эффективный сотрудник;',
            'Я горжусь, что я являюсь частью банка «Центр-инвест».'
        ]
    },
    {
        name: 'Акулин Павел',
        slogan: 'Кто ищет, тот всегда найдет!',
        post: 'Управление развития ИТ, в штате банка с сентября 2019 г.<br>программист',
        img: require('../assets/img/akulin.jpg'),
        voteResult: 'akulin',
        achievements: [
            'Освоил разработку в SAP «с нуля»;',
            'Усилил и повысил эффективность всей команды в решении задач развития обязательной ТОТ и аналитической отчетности;',
            'Активно участвует в проработке поставленных задач, предлагает способы и инструменты их решения.'
        ]
    },
    {
        name: 'Орловский Алексей',
        slogan: 'Помочь клиенту не просто оформить кредит, а воплотить в жизнь его заветную мечту',
        post: 'Управление розничного кредитования, в штате банка с марта 2020 г.<br>ведущий экономист по кредитованию',
        img: require('../assets/img/orlovskiy.jpg'),
        voteResult: 'orlovskiy',
        achievements: [
            'Уже стал ведущим экономистом;',
            'Всегда готов к любому заданию, составить сложнейший отчет для ЦБ? &ndash; Легко!;',
            'Дедлайны обходят стороной, может найти подход к самому притязательному клиенту.'
        ]
    },
    {
        name: 'Суховеев Дмитрий',
        slogan: 'Диджитализация, как неизбежность!',
        post: 'Управление развития ИТ, в штате банка с января 2019 г.<br>старший инженер-электронщик',
        img: require('../assets/img/suhoveev.jpg'),
        voteResult: 'suhoveev',
        achievements: [
            'Переработал основы организации рабочего места сотрудника по комплектации техникой ТОТ и эстетике в общем;',
            'Ввел новые правила по созданию информационных сетей;',
            'В команде инженеров уже показал свои работы на объектах банка: Переговорная Чехов, Учебный класс ЦПК и других помещениях внутри банка;',
            'Первый раз за 12 лет выполнено полное техническое обслуживание пневмопочты в головном офисе, ведет работы по оптимизации времени доставки капсул;',
            'Ведет работы по внедрению системы мониторинга источников бесперебойного питания.'
        ]
    },
    {
        name: 'Макаренко Никита',
        slogan: 'Хочешь жить – умей продавать',
        post: 'Управление розничных продуктов, в штате банка с июля 2020 г.<br>экономист отдела продаж',
        img: require('../assets/img/makarenko.jpg'),
        voteResult: 'makarenko',
        achievements: [
            'Доставка карт пенсионерам в тяжелое время пандемии;',
            'Участие в проекте «Карта болельщика ГК Ростов-Дон»;',
            'Для обеспечения комплексного обслуживания клиентов дополнительно прошел стажировки в смежных подразделениях: УРО, УРК, ОПЕРУ;',
            'Участие в турнирах по футболу, настольному теннису 2019-2020 гг.;',
            'Участие в информационных встречах с сотрудниками зарплатных организаций.'
        ]
    },
    {
        name: 'Бондарева Светлана',
        slogan: 'Без борьбы нет прогресса',
        post: 'Управление учета и отчетности, в штате банка с сентября 2020 г.<br>экономист отдела бухгалтерского учета и налогообложения',
        img: require('../assets/img/bondareva.jpg'),
        voteResult: 'bondareva',
        achievements: [
            'Прошла обучение и приступила к работе по своему участку в сжатые сроки и в условиях удаленной работы сотрудника, который передавал рабочий сектор;',
            'Светлана впервые организовала работу участка по подкреплению касс банка в удаленном режиме, ранее такие операции выполнялись только в офисе;',
            'Переработала выполняемые операции, оптимизировала свой участок работы, высвободив часть рабочего времени для замещения сотрудников и изучения нового материала.'
        ]
    },
    {
        name: 'Вдовина Анна',
        slogan: 'Новые компетенции и только вперед, далее &ndash; победа ждет!',
        post: 'Управление розничных операций, в штате банка с марта 2020 г.<br>экономист отдела клиентского обслуживания',
        img: require('../assets/img/vdovina.jpg'),
        voteResult: 'vdovina',
        achievements: [
            'Первое время успешно работала в контакт-центре, затем перешла на работу с клиентами физическими лицами, где успешно и за короткое время освоила новые направления;',
            'Целеустремленная, хочет помогать и быть полезной как коллективу, так и клиентам, очень клиентоориентирована, доброжелательна, готова осваивать новые участки работы;',
            'Участвовала в выдаче карт студентам ДГТУ, состояла в команде банка на фестивале науки, осуществляла исходящие кампании по продажам зарплатных карт организациям;',
            'Прошла тренинги ЦФГ: клиентское обслуживание, работа с возражениями, продажи.'
        ]
    },
    {
        name: 'Зайцев Виталий',
        slogan: 'Если кредитный сотрудник молчит, то лучше его не перебивать!',
        post: 'Управление кредитных рисков и мониторинга, в штате банка с августа 2019 г.<br>экономист отдела учета и мониторинга кредитных операций',
        img: require('../assets/img/zaycev.jpg'),
        voteResult: 'zaycev',
        achievements: [
            'Разработка (постановка задачи и тестирование) форм и логики отображения информации ТОТ о предстоящих платежах по кредитам для клиента в Банк-клиенте;',
            'Разработка автоматизированных электронных справок с ЭЦП о кредитной истории, ссудной задолженности и наличии гарантий для клиента в Банк-клиенте;',
            'Организация работы с внутренним архивом в соответствии с новым регламентом;',
            'Всегда готов подставить дружеское плечо и оказать помощь коллегам в крупных запросах регулятора, аудиторов и прочих рабочих моментах.'
        ]
    }
]
