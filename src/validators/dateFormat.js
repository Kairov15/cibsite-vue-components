import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.regex('dateFormat', /^([0-3][0-9].[0-1][0-9].(19|20)[0-9]{2})$/)
