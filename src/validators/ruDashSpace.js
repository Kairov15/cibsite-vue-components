import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.withParams({ type: 'ruDashSpace' }, (value) => {
    return !helpers.req(value) || /^([А-Яа-яёЁ.\-\s]+)$/g.test(value)
})
