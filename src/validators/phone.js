import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.regex('phone', /^(\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2})$/)
