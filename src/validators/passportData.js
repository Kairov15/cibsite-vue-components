import * as helpers from 'vuelidate/lib/validators/common'
export default helpers.regex('passportData', /^([0-9]{4}\s[0-9]{6})$/)
